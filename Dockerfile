FROM alpine:latest
## Base packages
RUN apk add --no-cache python3
RUN apk update
RUN apk add --virtual build-dependencies build-base gcc wget git
## Build dependencies
RUN apk add python3-dev
RUN apk add libffi-dev
RUN apk add openssl-dev
RUN python3 -m ensurepip
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --upgrade setuptools
RUN python3 -m pip install --upgrade pysftp==0.2.9
RUN python3 -m pip install --upgrade pyftp==0.1.0


VOLUME /data/out/tables
VOLUME /config

ADD src/ /code

WORKDIR /code

# Edit this any way you like it
ENTRYPOINT ["python3", "main.py"]
