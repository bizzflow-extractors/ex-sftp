#!/usr/bin/env python3

## Requires pysftp=0.2.9

import os
import json
import csv
import pysftp
import sys
import codecs
from math import floor
from ftplib import FTP, FTP_TLS
import ssl

TEMP_DIR = "/tmp"
OUTPUT_DIR = "/data/out/tables"
PEEK_SIZE = 4096
INPUT_ENCODING = "utf-8"

if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

## Config
if len(sys.argv)==2:
    config_path = sys.argv[1]
else:
    #config_path = "/config/config.json"
    config_path = "/config/config.json"

if not os.path.exists(config_path):
    raise Exception("Configuration file not found")

with open(config_path) as conf_file:
    conf = json.loads(conf_file.read())
    if "encoding" in conf and conf["encoding"]:
        INPUT_ENCODING = conf["encoding"]
        print("Asuming default encoding {}".format(INPUT_ENCODING))

## Display debug info only if wanted
def debug(*args):
    if conf["debug"]:
        print(*args)

## Normalize output filename
def path_normalize(path):
    file_ext = os.path.splitext(path)[1]
    normalized = (((path[:-len(file_ext)]).replace("/","_").replace("\\","_").replace("-","_").replace(".","_"))+file_ext).lower()
    return normalized

## Pretty progress bar
def progress_bar(percentage):
    arcs = int(floor(20*percentage))
    fill_in = ("#"*arcs)+(" "*(20-arcs))
    return fill_in

# If the file encoded in UTF-8 with BOM
def remove_bom(path):
    BUFSIZE = 4096
    BOMLEN = len(codecs.BOM_UTF8)

    #binary downloaded files
    with open(path, "r+b") as fp:
        chunk = fp.read(BUFSIZE)
        if chunk.startswith(codecs.BOM_UTF8):
            i = 0
            chunk = chunk[BOMLEN:]
            while chunk:
                fp.seek(i)
                fp.write(chunk)
                i += len(chunk)
                fp.seek(BOMLEN, os.SEEK_CUR)
                chunk = fp.read(BUFSIZE)
            fp.seek(-BOMLEN, os.SEEK_CUR)
            fp.truncate()

    # string downloaded files
    with open(path, "r", encoding=INPUT_ENCODING) as fin:
        if fin.read(3) == 'ï»¿':
            lines = fin.readlines()
            lines[0] = lines[0][0:]
            bom = True
        else:
            bom = False
    if bom:
        with open(path, "w", encoding="utf-8") as fout:
            fout.writelines(lines)
    

## Convert CSVs not conforming to UNIX standard
def convert_csv(path, original_path=None):
    remove_bom(path)
    include_filename = ("include_filename" in conf and conf["include_filename"])
    if original_path is None:
        original_path = path
    sniffer = csv.Sniffer()
    base_name = os.path.basename(path)
    original_filename = os.path.basename(original_path)
    output_path = os.path.join(OUTPUT_DIR, base_name)
    with open(path, encoding=INPUT_ENCODING) as infile:
        peek_data = infile.read(PEEK_SIZE)
        dialect = sniffer.sniff(peek_data)
        ## file is invalid or we are to append filename column
        if dialect.delimiter != csv.unix_dialect.delimiter or dialect.escapechar != csv.unix_dialect.escapechar or dialect.lineterminator != csv.unix_dialect.lineterminator or dialect.quoting != csv.unix_dialect.quoting or include_filename:
            infile.seek(0)
            reader = csv.reader(infile, dialect=dialect)
            with open(output_path, "w", encoding="utf-8") as outfile:
                writer = csv.writer(outfile, dialect=csv.unix_dialect)
                if include_filename:
                    writer.writerows([[*line, "original_filename" if lineno == 0 else original_filename] for lineno, line in enumerate(reader)])
                else:
                    writer.writerows(reader)
            return
    ## file is valid
    os.rename(path, output_path)

## FTP connection class
class FTPConnection():
    def __init__(self, host, user, password, port=21):
        self.connection = FTP()
        self.connection.connect(host, port)
        self.connection.login(user=user, passwd=password)

    def get(self, path, output_filename, downolad_callback=None):
        with open(output_filename, "wb") as fhandle:
            self.connection.retrbinary("RETR {}".format(path), fhandle.write)

    def listdir(self, path=None):
        dirlist = []
        self.connection.dir(path, dirlist.append)
        names = [i[56:] for i in dirlist]
        names.sort(key=lambda i: i.lower())
        return names

    def isfile(self, path):
        try:
            x = self.connection.size(path)
        except Exception as e:
            x = None
        if isinstance(x, int):
            return True
        else:
            return False

    def isdir(self, path):
        try:
            x = self.connection.size(path)
        except Exception as e:
            x = None
        if isinstance(x, int):
            return False
        else:
            if self.listdir(path):
                return True

    def exists(self, path):
        if self.isdir(path) or self.isfile(path):
            return True
        else:
            return False

## FTPS connection class
class FTPSConnection():
    def __init__(self, host, user, password, port=2121):
        self.connection = FTP_TLS()
        self.connection.ssl_version = ssl.PROTOCOL_TLS
        self.connection.connect(host, port)
        self.connection.login(user=user, passwd=password)
        self.connection.prot_p()

    def get(self, path, output_filename, downolad_callback=None):
        '''
        with open(output_filename, 'wb') as f:
            self.connection.retrbinary('RETR {}'.format(path), f.write)
        ''' 
        with open(output_filename, "wt", encoding="utf-8") as fhandle:
            self.connection.retrlines("RETR {}".format(path), lambda s, w = fhandle.write: w(s +  "\n"))

    def listdir(self, path = None):
        names = self.connection.nlst(path)
        return names

    def isfile(self, path):
        try:
            x = self.connection.size(path)
        except Exception as e:
            x = None
        if isinstance(x, int):
            return True
        else:
            return False

    def isdir(self, path):
        try:
            x = self.connection.size(path)
        except Exception as e:
            x = None
        if isinstance(x, int):
            return False
        else:
            if self.listdir(path):
                return True

    def exists(self, path):
        if self.isdir(path) or self.isfile(path):
            return True
        else:
            return False

## Main Class
class SFTPExtractor():
    def __init__(self):
        self.connection = None
        self.connection_options = pysftp.CnOpts()
        self.connection_options.hostkeys = None
        if conf["private_key"]!="":
            with open("private_key","w") as keyfile:
                keyfile.write(conf["private_key"])
            conf["private_key"] = "private_key"
            conf["password"] = None
            if conf["private_key_pass"]=="":
                conf["private_key_pass"] = None
        else:
            conf["private_key"] = None
            conf["private_key_pass"] = None

    def connect(self):
        if "ftp" in conf and conf["ftp"]:
            try:
                ftpcon = FTPConnection(
                    host=conf["host"],
                    user=conf["username"],
                    password=conf["password"],
                    port=conf["port"]
                )
                self.connection = ftpcon
                return True
            except Exception as e:
                print(e)
                print("Could not connect")
                raise Exception(e)
            return False
        elif "ftps" in conf and conf["ftps"]:
            try:
                ftpcon = FTPSConnection(
                    host=conf["host"],
                    user=conf["username"],
                    password=conf["password"],
                    port=conf["port"]
                )
                self.connection = ftpcon
                print("Connected to {} as {}".format(conf["host"], conf["username"]))
                return True
            except Exception as e:
                print(e)
                print("Could not connect")
                raise Exception(e)
            return False
        else:
            try:
                self.connection = pysftp.Connection(
                    host=conf["host"],
                    username=conf["username"],
                    port=conf["port"],
                    private_key=conf["private_key"],
                    private_key_pass=conf["private_key_pass"],
                    password=conf["password"],
                    cnopts=self.connection_options
                    )
                return True
            except Exception as e:
                print(e)
                print("Could not connect")
                raise Exception(e)
            return False
    def download_dir(self,dpath):
        """
        Walk through remote directory and add all files to download list
        """
        ## Error handling
        if self.connection == None:
            raise Exception("Not connected")
        if not self.connection.exists(dpath):
            raise Exception("Remote directory \'{0}\' not found.".format(dpath))
        if not self.connection.isdir(dpath):
            raise Exception("Remote \'{0}\' is not a directory.".format(dpath))
        print("Downloading directory {0}".format(dpath))
        ## Process files and directories
        if "ftps" in conf and conf["ftps"]:
            for item_path in self.connection.listdir(dpath):
                if self.connection.isdir(item_path):
                    if conf["recursive"]:
                        self.download_dir(item_path)
                else:
                    self.download_file(item_path)
        else:
            for item in self.connection.listdir(dpath):
                item_path = os.path.join(dpath,item)
                print(item_path)
                if self.connection.isdir(item_path):
                    if conf["recursive"]:
                        self.download_dir(item_path)
                else:
                    self.download_file(item_path)
    def download_file(self,path):
        """
        Download remote file locally
        """
        ## Check for supported type
        if not path.lower().endswith(".csv"):
            debug("Skipping {0}, not a .csv file".format(path))
            return False
        ## Error handling
        if self.connection == None:
            raise Exception("Not connected")
        if not self.connection.exists(path):
            raise Exception("Remote file \'{0}\' not found.".format(path))
        if not self.connection.isfile(path):
            raise Exception("Remote \'{0}\' is not a file.".format(path))
        output_filename = os.path.join(TEMP_DIR, path_normalize(path))
        
        from socket import error as SocketError
        import errno

        try:
            print("Downloading file {0} as {1}".format(path,output_filename))
            self.connection.get(path,output_filename,self.download_callback)
        except SocketError as e:
            if e.errno != errno.ECONNRESET:
                raise Exception(e)# Not error we are looking for
            print("[Errno 104] Connection reset by peer. Reconnecting...")
            self.connect()
            print("Again downloading file {0} as {1}".format(path,output_filename))
            self.connection.get(path,output_filename,self.download_callback)
        print("Converting to UNIX-style CSV...")
        convert_csv(output_filename, path)
        if conf["progress"]: print("")
    def download_callback(self,received,total):
        if not conf["progress"]: return
        perc = float(received)/float(total)
        fill_in = progress_bar(perc)
        print("Downloading {0} {1} %".format(fill_in,floor(100*perc)),end='\r')

    def extract(self):
        ## Error handling
        if self.connection == None:
            raise Exception("Not connected")
        for item in conf["download"]:
            if not self.connection.exists(item):
                raise Exception("Remote item \'{0}\' not found.".format(item))
            if self.connection.isfile(item):
                self.download_file(item)
            else:
                self.download_dir(item)

extractor = SFTPExtractor()
extractor.connect()
extractor.extract()
