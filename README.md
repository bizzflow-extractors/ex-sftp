# BizzFlow SFTP Extractor

Extractor extracts .csv files in specified SFTP remote folder. Extractor supports either private key and password protection.
Extractors also support FTP.

## Sample config

```json
{
    "private_key": "--BEGIN RSA...--\nKEY CONTENT",
    "private_key_pass": "",
    "host": "remote.host.sftp.com",
    "username": "bizztreat",
    "password": "",
    "port": 22,
    "recursive": true,
    "debug": true,
    "progress": true,
    "ftp": false,
    "ftps": true,
    "download": [
        "/home/bizztreat/upload/",
        "/home/bizztreat/single-files/catalog.csv"
    ],
    "include_filename": false,
    "encoding": "utf-8"
}
```

### Config explanation

| Parameter | Type | Description |
| --- | --- | --- |
| private_key | string | Private key content coppied from the key file |
| private_key_pass | string | Private key password (can be empty) |
| username | string | remote host user name |
| password | string | Password (can be ommitted if connecting using private_key) |
| host | string | Remote host hostname or IP address |
| port | int | SFTP remote port (default: 22) |
| recursive | boolean | If a directory is among downloaded paths, should we download all subdirectories as well? |
| debug | boolean | Print more detailed info |
| progress | boolean | Whether to print progress bar |
| ftp | boolean | Whether to use FTP instead of SFTP and FTP |
| ftps | boolean | Whether to use FTPS instead of SFTP and FTP |
| download | list of strings | List of paths (files and directories) to download. For directories, all files are listed and only .csv files are downloaded |
| include_filename | boolean, null or missing | if set to `true`, CSV will have column `original_filename` appended to them; can be omitted |
| encoding | string, null or missing | use this encoding to open remote csv files (files will always be exported as utf-8), default: utf-8 |
